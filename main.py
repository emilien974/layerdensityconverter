# -*- coding: utf-8 -*-

# auteur : Emilien Basset
# date : 6/12/2018
# email : basset.emilien@gmail.com
from PIL import Image
from random import randint
import os

def modify_border(path):
    imgs = [Image.open(p).convert('L') for p in path]
    pixs = [img.load() for img in imgs]

    for y in range(imgs[0].size[0]):
        for x in range(imgs[0].size[1]):
            for p in range(len(path)):
                if max([pixs[n][x,y] for n in range(len(path))])==pixs[p][x,y]:
                    pixs[p][x,y]=255
                    for p2 in range(len(path)):
                        if p2!=p:
                            pixs[p2][x,y]=0
                break
                
                    
    for n in range(len(imgs)):
        imgs[n].save(path[n])
        
    

def interaction(listLayers):
    print("voici les layers qui ont été detecté")
    for n in range(len(listLayers)):
        print(n,listLayers[n])

    priority = input("indiquez un ordre de priorité des layers : ").split(",")
    return [int(n) for n in range(len(priority))]

def conversion(path,cut):
    in1 = Image.open(path).convert('L')

    outs = [Image.new("L",in1.size) for x in range(sp)]

    pixin = in1.load()
    pixouts = [outs[n].load() for n in range(sp)]

    for y in range(in1.size[0]):
        for x in range(in1.size[1]): # coordonnee
            if pixin[x,y]!=0:
                rand = randint(0,sp-1)
                for n in range(sp): # quelle image se voit attribuer le pixel blanc
                    if n==rand:
                        pixouts[n][x,y]=255
                        

    for n in range(sp):
        outs[n].save("output/"+path[:-11]+format(n+1,"02")+"_weight.png", "PNG")

if __name__=="__main__":
    sp = int(input("en combien de fois il faut couper les images ? "))

    listLayers = []
    for e in os.listdir():
        if e[-11:]=="_weight.png":
            listLayers.append(e)

    modify = input("voulez vous rectifier les jointure de layers ?(O/n) ")
    if modify in ('o','O',''):
        #pri = interaction(listLayers)
        modify_border(listLayers)

    for e in listLayers:
        conversion(e,sp)
