# layerDensityConverter

Terrain layer density converter from farming simulator 2017 to farming simulator 2019.

## Français

	Ce programme sert à convertir des calques de terrain provenant d'un map pour farming
	simulator 2017 vers farming simulator 2019. c'est un outil qui peut s'avérer utile pour les moddeurs.
	Le résultat n'est pas exactement comme attendu mais ça reste assez fidèle tout de même.
	
### prérequis

	pour pouvoir executer le programme, il est necessaire d'avoir un interpreteur python3 ainsi que le
	module Pillow d'installé.
	vous pourrez trouver un bon interpreteur sur le site officiel : 
	* https://www.python.org/downloads/
		
	
	pour le module Pillow, plusieurs solutions s'offrent à vous, personnellement, je suggère d'installer
	pip si il celui ci n'est pas installé de base pour ensuite installer Pillow.
	ouvrir un cmd puis lancez python3 via la commande suivante :
	
	```
	python -m pip install Pillow
	```
	
	*Note* : si l'interpreteur vous dit que pip n'est pas à jour, executez ceci :
	
	```
	python -m pip install --upgrade-pip
	```
	
### mode d'emploi

	Mettre tout vos fichier _weight.png dans le même dossier que le programme. Au lancement, indiquer le nombre
	de coupe du/des layers.
	vos images ont tout été convertit.
	
	*Note* : sur farming simulator, les layers sont coupés en 4, en realité, seul les 3 premier sont actif
	et le dernier reste noir. En conséquence, lorsque vous allez executer le programme, pensez à specifier
	une coupe en 3 pour ne pas avoir de problèmes.
	 
	Aussi, les fichier _weight.png de fs17 sont dimensionnés en 1024x1024. pensez à les mettre en 2048x2048 à
	l'aide d'un autre logiciel.

	
	
## English

	This program can convert terrain layer from fs17 map to fs19 map. This is a tool for modder.
	
### prérequis

	to be able to execute the program, it is necessary to have a python interpreter3 as well as the
	Pillow module installed.
	you can find a good interpreter on the official site :
	* https://www.python.org/downloads/
		
	
	for the Pillow module, several solutions are available to you, personally, I suggest to install
	pip if it is not installed base then install Pillow.
	open a cmd then run python3 via the following command :
	
	```
	python -m pip install Pillow
	```
	
	*Note* : if the interpreter tells you that pip is out of date, run this :
	
	```
	python -m pip install --upgrade-pip
	```

### How to

	Put all your _weight.png files in the same folder as the program. At launch, indicate the number
	cutting the layers.
	all your layers have been converted.
	
	*Note* : on farming simulator, the layers are cut in 4, in reality, only the first 3 are active
	and the last one remains black. As a result, when you are going to run the program, think about specifying
	a cut in 3 to not have problems.
	
	Also, the _weight.png files of fs17 are sized to 1024x1024. think about putting them in 2048x2048 to
	using another software.